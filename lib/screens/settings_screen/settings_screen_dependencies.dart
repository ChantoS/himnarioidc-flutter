// Flutter packages
export 'package:flutter/material.dart';

// Default widgets
export 'default_widgets/build_settings_screen_appbar.dart';
export 'default_widgets/build_settings_screen_body.dart';

// Structure
export 'package:himnarioidc/screens/settings_screen/structure/build_change_version_button.dart';
export 'package:himnarioidc/screens/settings_screen/structure/build_quick_info.dart';

// Dependencies
export 'package:himnarioidc/app_dependencies.dart';
