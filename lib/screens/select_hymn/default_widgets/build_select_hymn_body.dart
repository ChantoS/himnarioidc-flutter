import 'package:himnarioidc/app_dependencies.dart';
import 'package:himnarioidc/screens/select_hymn/logic/popup.dart';
import 'package:himnarioidc/screens/select_hymn/structure/build_popup.dart';

class BuildSelectHymnBody extends StatefulWidget {
  const BuildSelectHymnBody({Key? key}) : super(key: key);

  @override
  State<BuildSelectHymnBody> createState() => _BuildSelectHymnBodyState();
}

class _BuildSelectHymnBodyState extends State<BuildSelectHymnBody> {
  PopupLogic popupLogic = PopupLogic();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: SharedPreferencesManager.isFirstTime(),
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasData) {
          bool isFirstTime = snapshot.data!;
          if (!isFirstTime) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              popupLogic.showDownloadingDialog(
                  context, "Buscando actualizaciones", false);
              Future.delayed(const Duration(seconds: 4), () async {
                await Future.delayed(Duration.zero);
                Navigator.of(context).pop();
                Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                    builder: (BuildContext context) => const NavigationHandle(),
                  ),
                  (Route<dynamic> route) => false,
                );
              });
            });
          }

          return buildPopup(context);
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
