import 'package:himnarioidc/app_dependencies.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  populateDatabase();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          appBarTheme: const AppBarTheme(
            iconTheme: IconThemeData(color: Color(0xFF1E2A47)),
          ),
        ),
        debugShowCheckedModeBanner: false,
        home: const SelectHymnScreen(),
        routes: {
          '/song': (context) => const SongScreen(),
        });
  }
}
